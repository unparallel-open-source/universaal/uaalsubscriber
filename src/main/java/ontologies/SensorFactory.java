package ontologies;

import org.universAAL.middleware.rdf.Resource;
import org.universAAL.middleware.rdf.ResourceFactory;

public class SensorFactory implements ResourceFactory {

	@Override public Resource createInstance(String classURI, String instanceURI, int factoryIndex) {
		switch(factoryIndex) {
			case 0: return new HumiditySensor(instanceURI);
			case 1: return new SPLFastSensor(instanceURI);
			case 2: return new SPLSlowSensor(instanceURI);
			default: return null;
		}
	}
}