package ontologies;

import org.universAAL.ontology.device.DeviceOntology;
import org.universAAL.ontology.device.Sensor;

public class SPLSlowSensor extends Sensor {

	public static final String MY_URI = DeviceOntology.NAMESPACE + "SPLSlowSensor";
	public static final String PROP_HAS_VALUE = SPLSlowSensorOntology.NAMESPACE + "hasValue";
	public static final String PROP_HAS_TYPE = SPLSlowSensorOntology.NAMESPACE + "hasType";
	public static final String PROP_SPL = SPLSlowSensorOntology.NAMESPACE + "splslow";
	
	public SPLSlowSensor() {
		super();
	}
	
	public SPLSlowSensor(String instanceURI) {
		super(instanceURI);
	}
	
	public String getClassURI() {
		return MY_URI;
	}

	public int getPropSerializationType(String propURI) {
		return PROP_HAS_VALUE.equals(propURI) ? PROP_SERIALIZATION_FULL : super.getPropSerializationType(propURI);
	}

	public boolean isWellFormed() {
		return true;
	}
}
