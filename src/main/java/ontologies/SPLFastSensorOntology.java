package ontologies;

import org.universAAL.middleware.owl.IntRestriction;
import org.universAAL.middleware.owl.MergedRestriction;
import org.universAAL.middleware.owl.OntClassInfoSetup;
import org.universAAL.middleware.owl.Ontology;
import org.universAAL.middleware.rdf.Resource;
import org.universAAL.ontology.device.DeviceOntology;
import org.universAAL.ontology.device.Sensor;

public class SPLFastSensorOntology extends Ontology {

	public static final String NAMESPACE = "http://ontology.universAAL.org/Device.owl#";
	
	private static SensorFactory sensorFactory = new SensorFactory();
	
	public SPLFastSensorOntology() {
		super(NAMESPACE);
	}

	@Override public void create() {
		Resource resource = getInfo();
		resource.setResourceComment("Sensor Event");
		resource.setResourceLabel("Sensor");
		addImport(DeviceOntology.NAMESPACE);
		
		OntClassInfoSetup ontologyInfo = createNewOntClassInfo(SPLFastSensor.MY_URI, sensorFactory, 1);
		ontologyInfo.setResourceComment("SPL Fast Sensor Event");
		ontologyInfo.setResourceLabel("SPL Fast Sensor");
		ontologyInfo.addSuperClass(Sensor.MY_URI);
		ontologyInfo.addObjectProperty(SPLFastSensor.PROP_HAS_VALUE).setFunctional();
		ontologyInfo.addDatatypeProperty(SPLFastSensor.PROP_SPL).setFunctional();
		ontologyInfo.addRestriction(MergedRestriction.getAllValuesRestriction(SPLFastSensor.PROP_HAS_VALUE, SPLFastSensor.MY_URI));
		ontologyInfo.addRestriction(MergedRestriction.getAllValuesRestriction(SPLFastSensor.PROP_SPL, new IntRestriction(0, true, 100, true)));
	}
}
