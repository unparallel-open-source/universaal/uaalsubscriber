package ontologies;

import org.universAAL.middleware.owl.IntRestriction;
import org.universAAL.middleware.owl.MergedRestriction;
import org.universAAL.middleware.owl.OntClassInfoSetup;
import org.universAAL.middleware.owl.Ontology;
import org.universAAL.middleware.rdf.Resource;
import org.universAAL.ontology.device.DeviceOntology;
import org.universAAL.ontology.device.Sensor;

public class HumiditySensorOntology extends Ontology {

	public static final String NAMESPACE = "http://ontology.universaal.org/Device.owl#";
	
	private static SensorFactory sensorFactory = new SensorFactory();
	
	public HumiditySensorOntology() {
		super(NAMESPACE);
	}

	@Override public void create() {
		Resource resource = getInfo();
		resource.setResourceComment("Sensor Event");
		resource.setResourceLabel("Sensor");
		addImport(DeviceOntology.NAMESPACE);
		
		OntClassInfoSetup ontologyInfo = createNewOntClassInfo(HumiditySensor.MY_URI, sensorFactory, 0);
		ontologyInfo.setResourceComment("Humidity Sensor Event");
		ontologyInfo.setResourceLabel("Humidity Sensor");
		ontologyInfo.addSuperClass(Sensor.MY_URI);
		ontologyInfo.addObjectProperty(HumiditySensor.PROP_HAS_VALUE).setFunctional();
		ontologyInfo.addDatatypeProperty(HumiditySensor.PROP_HUMIDITY).setFunctional();
		ontologyInfo.addRestriction(MergedRestriction.getAllValuesRestriction(HumiditySensor.PROP_HAS_VALUE, HumiditySensor.MY_URI));
		ontologyInfo.addRestriction(MergedRestriction.getAllValuesRestriction(HumiditySensor.PROP_HUMIDITY, new IntRestriction(0, true, 100, true)));
	}
}
