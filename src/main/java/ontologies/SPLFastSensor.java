package ontologies;

import org.universAAL.ontology.device.DeviceOntology;
import org.universAAL.ontology.device.Sensor;

public class SPLFastSensor extends Sensor {

	public static final String MY_URI = DeviceOntology.NAMESPACE + "SPLFastSensor";
	public static final String PROP_HAS_VALUE = SPLFastSensorOntology.NAMESPACE + "hasValue";
	public static final String PROP_HAS_TYPE = SPLFastSensorOntology.NAMESPACE + "hasType";
	public static final String PROP_SPL = SPLFastSensorOntology.NAMESPACE + "splfast";
	
	public SPLFastSensor() {
		super();
	}
	
	public SPLFastSensor(String instanceURI) {
		super(instanceURI);
	}
	
	public String getClassURI() {
		return MY_URI;
	}

	public int getPropSerializationType(String propURI) {
		return PROP_HAS_VALUE.equals(propURI) ? PROP_SERIALIZATION_FULL : super.getPropSerializationType(propURI);
	}

	public boolean isWellFormed() {
		return true;
	}
}
