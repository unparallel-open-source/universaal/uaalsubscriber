package subscriber;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;

import org.universAAL.middleware.brokers.message.space.SpaceMessage;
import org.universAAL.middleware.bus.member.BusMember;
import org.universAAL.middleware.bus.model.AbstractBus;
import org.universAAL.middleware.connectors.util.ChannelMessage;
import org.universAAL.middleware.container.ModuleContext;
import org.universAAL.middleware.context.ContextEvent;
import org.universAAL.middleware.context.ContextEventPattern;
import org.universAAL.middleware.context.ContextSubscriber;
import org.universAAL.middleware.interfaces.PeerCard;
import org.universAAL.middleware.interfaces.space.SpaceCard;
import org.universAAL.middleware.interfaces.space.SpaceDescriptor;
import org.universAAL.middleware.modules.CommunicationModule;
import org.universAAL.middleware.modules.SpaceModule;
import org.universAAL.middleware.modules.exception.CommunicationModuleException;
import org.universAAL.middleware.modules.listener.MessageListener;
import org.universAAL.middleware.owl.MergedRestriction;
import org.universAAL.middleware.service.ServiceCaller;
import org.universAAL.ontology.device.HumiditySensor;
import org.universAAL.ontology.device.TemperatureSensor;
import org.universAAL.ontology.device.ValueDevice;
import org.universAAL.ontology.healthmeasurement.owl.HeartRate;

import ontologies.SPLFastSensor;
import ontologies.SPLSlowSensor;

public class Subscriber extends ContextSubscriber {
	private static final String STRING_FORMATTER = "|%1$22s|%2$13s|%3$9s|\n";
	
	private List<ContextEvent> lastEvents;
	//private SubscriberGUI gui;
	private boolean isHeaderSet;
	
	public Subscriber(ModuleContext context) {
		super(context, getPermanentSubscriptions());
		lastEvents = new ArrayList<ContextEvent>();
		isHeaderSet = false;
		//gui = new SubscriberGUI();
		System.out.println("Subscriber created\n");
	}

	private static ContextEventPattern[] getPermanentSubscriptions() {
		System.out.println("Building restrictions...");
		ContextEventPattern heartRatePattern = new ContextEventPattern();
		ContextEventPattern temperaturePattern = new ContextEventPattern();
		ContextEventPattern humidityPattern = new ContextEventPattern();
		/*ContextEventPattern splSlowPattern = new ContextEventPattern();
		ContextEventPattern splFastPattern = new ContextEventPattern();
		*/
		heartRatePattern.addRestriction(MergedRestriction.getAllValuesRestriction(ContextEvent.PROP_RDF_SUBJECT, HeartRate.MY_URI));
		heartRatePattern.addRestriction(MergedRestriction.getFixedValueRestriction(ContextEvent.PROP_RDF_SUBJECT, new HeartRate("urn:org.universAAL.rest:HeartRate")));
		heartRatePattern.addRestriction(MergedRestriction.getFixedValueRestriction(ContextEvent.PROP_RDF_PREDICATE, ValueDevice.PROP_HAS_VALUE));
		
		temperaturePattern.addRestriction(MergedRestriction.getAllValuesRestriction(ContextEvent.PROP_RDF_SUBJECT, TemperatureSensor.MY_URI));
		temperaturePattern.addRestriction(MergedRestriction.getFixedValueRestriction(ContextEvent.PROP_RDF_SUBJECT, new TemperatureSensor("urn:org.universAAL.rest:Temperature")));
		temperaturePattern.addRestriction(MergedRestriction.getFixedValueRestriction(ContextEvent.PROP_RDF_PREDICATE, ValueDevice.PROP_HAS_VALUE));
		
		humidityPattern.addRestriction(MergedRestriction.getAllValuesRestriction(ContextEvent.PROP_RDF_SUBJECT, HumiditySensor.MY_URI));
		humidityPattern.addRestriction(MergedRestriction.getFixedValueRestriction(ContextEvent.PROP_RDF_SUBJECT, new HumiditySensor("urn:org.universAAL.rest:Humidity")));
		humidityPattern.addRestriction(MergedRestriction.getFixedValueRestriction(ContextEvent.PROP_RDF_PREDICATE, ValueDevice.PROP_HAS_VALUE));
		
		/*splFastPattern.addRestriction(MergedRestriction.getAllValuesRestriction(ContextEvent.PROP_RDF_SUBJECT, SPLFastSensor.MY_URI));
		splFastPattern.addRestriction(MergedRestriction.getFixedValueRestriction(ContextEvent.PROP_RDF_SUBJECT, new SPLFastSensor("urn:org.universAAL.rest:SPLFast")));
		splFastPattern.addRestriction(MergedRestriction.getFixedValueRestriction(ContextEvent.PROP_RDF_PREDICATE, ValueDevice.PROP_HAS_VALUE));
		
		splSlowPattern.addRestriction(MergedRestriction.getAllValuesRestriction(ContextEvent.PROP_RDF_SUBJECT, SPLSlowSensor.MY_URI));
		splSlowPattern.addRestriction(MergedRestriction.getFixedValueRestriction(ContextEvent.PROP_RDF_SUBJECT, new SPLSlowSensor("urn:org.universAAL.rest:SPLSlow")));
		splSlowPattern.addRestriction(MergedRestriction.getFixedValueRestriction(ContextEvent.PROP_RDF_PREDICATE, ValueDevice.PROP_HAS_VALUE));*/
		
		return new ContextEventPattern[] { heartRatePattern, temperaturePattern, humidityPattern, /*splSlowPattern, splFastPattern*/ };
	}

	public void communicationChannelBroken() {
		close();
		System.out.println("Subscriber's Communication Channel is now broken!");
	}

	public void handleContextEvent(ContextEvent event) {
		//lastEvents.add(event);
		//gui.updateHeartRateValues(event.getRDFObject().toString());
		if(!isHeaderSet) {
			
			System.out.println("\n------------------------------------------------");
			System.out.format(STRING_FORMATTER, "     Timestamp      ", "   Type    ", " Data  ");
			System.out.println("------------------------------------------------");
			isHeaderSet = true;
		}
		
		ZonedDateTime datetime = ZonedDateTime.ofInstant(Instant.now(), ZoneId.systemDefault());
		String timestamp = datetime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'"));
		/* timestamp, dataType, value */
		/* Timestamp | Type | Data */

		if(event.getSubjectURI().equals("urn:org.universAAL.rest:HeartRate")) {
			System.out.format(STRING_FORMATTER, " " + timestamp + " ", "  Heart Rate ", " " + event.getRDFObject() + " bpm ");
		} else if(event.getSubjectURI().equals("urn:org.universAAL.rest:Temperature")) {
			System.out.format(STRING_FORMATTER, " " + timestamp + " ", " Temperature ", " " + event.getRDFObject() + " ºC ");
		} else if(event.getSubjectURI().equals("urn:org.universAAL.rest:Humidity")) {
			System.out.format(STRING_FORMATTER, " " + timestamp + " ", " Humidity ", " " + event.getRDFObject() + " % ");
		} else if(event.getSubjectURI().equals("urn:org.universAAL.rest:SPLFast")) {
			System.out.format(STRING_FORMATTER, " " + timestamp + " ", " SPL Fast ", " " + event.getRDFObject() + " db ");
		} else if(event.getSubjectURI().equals("urn:org.universAAL.rest:SPLSlow")) {
			System.out.format(STRING_FORMATTER, " " + timestamp + " ", " SPL Slow ", " " + event.getRDFObject() + " db ");
		}
		
		
		//System.out.println("Event Resource Label: " + event.getResourceLabel());
		//System.out.println("Event received [Type, Subject, Predicate, Object] @ " + timestamp + ": [" + event.getSubjectTypeURI() + ", " + event.getSubjectURI() + ", " + event.getRDFPredicate() + ", " + event.getRDFObject() + "]");
	}
}
