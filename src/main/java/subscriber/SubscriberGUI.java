package subscriber;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

public class SubscriberGUI extends JPanel {
	private static final long serialVersionUID = 3593386450351243407L;
	
	//private Activator activator;
	private JFrame frame;
	private JLabel[] titleLabels, heartRateValues, weightValues, temperatureValues, humidityValues, splValues;
	
	private JLabel label1 = new JLabel();
	
	public SubscriberGUI(/*Activator activator*/) {
		super();
		//this.activator = activator;
		titleLabels = new JLabel[5];
		heartRateValues = new JLabel[20];
		weightValues = new JLabel[20];
		temperatureValues = new JLabel[20];
		humidityValues = new JLabel[20];
		splValues = new JLabel[20];
		initializeArrays();
		startGUI();
		
		System.out.println("\n\nSubscriber GUI Created\n\n");
		
		//dummyValueGenerator();
	}
	
	private void startGUI() {
		frame = new JFrame("OCARIOT Demonstrator GUI");
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.pack();
		frame.setSize(1200, 800);
		frame.setVisible(true);
		frame.getContentPane().setLayout(null);
		frame.setEnabled(true);	
		frame.setResizable(false);
		
		setupLabels();
	}
	
	private void setupLabels() {
		int y = 75;
		for(JLabel label : titleLabels) {
			frame.getContentPane().add(label);
			// x, y, width, height
			label.setBounds(75, y, 150, 150);
			y += 75;
		}
		
		titleLabels[0].setFont(new Font("Dialog", Font.PLAIN, 20));
		titleLabels[1].setFont(new Font("Dialog", Font.PLAIN, 20));
		titleLabels[2].setFont(new Font("Dialog", Font.PLAIN, 20));
		titleLabels[3].setFont(new Font("Dialog", Font.PLAIN, 20));
		titleLabels[4].setFont(new Font("Dialog", Font.PLAIN, 20));
		
		titleLabels[0].setText("Heart Rate");
		titleLabels[1].setText("Weight");
		titleLabels[2].setText("Temperature");
		titleLabels[3].setText("Humidity");
		titleLabels[4].setText("SPL");
		
		int x = 250;
		for(JLabel label : heartRateValues) {
			frame.getContentPane().add(label);
			// x, y, width, height
			label.setBounds(x, 75, 150, 150);
			x += 45;
		}
	}
	
	private void initializeArrays() {
		for(int i = 0; i < titleLabels.length; i++) {
			titleLabels[i] = new JLabel();
		}
		
		for(int i = 0; i < heartRateValues.length; i++) {
			heartRateValues[i] = new JLabel();
			weightValues[i] = new JLabel();
			temperatureValues[i] = new JLabel();
			humidityValues[i] = new JLabel();
			splValues[i] = new JLabel();
		}
	}
	
	public void updateHeartRateValues(String value) {
		String previousValue;
		for(int i = heartRateValues.length - 1; i > 0; i--) {
			previousValue = heartRateValues[i - 1].getText();
			heartRateValues[i].setText(previousValue);
			heartRateValues[i].setFont(new Font("Dialog", Font.PLAIN, 16));
		}
		
		heartRateValues[0].setText(value);
		heartRateValues[0].setFont(new Font("Dialog", Font.PLAIN, 16));
	}
	
	private void dummyValueGenerator() {
		for(int i = 0; i < 10; i++) {
			updateHeartRateValues(Integer.toString(i));
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void updateGUI(String value) {
		label1.setText(value);
	}
}