package subscriber;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.universAAL.middleware.container.ModuleContext;
import org.universAAL.middleware.container.osgi.OSGiContainer;
import org.universAAL.middleware.owl.OntologyManagement;

import ontologies.HumiditySensorOntology;
import ontologies.SPLFastSensorOntology;
import ontologies.SPLSlowSensorOntology;

public class Activator implements BundleActivator {
	//public static BundleContext osgiContext = null;
	public static ModuleContext moduleContext;
	//private SubscriberGUI gui;
	private Subscriber subscriber;

	public void start(BundleContext bcontext) throws Exception {
		//Activator.osgiContext = bcontext;
		moduleContext = OSGiContainer.THE_CONTAINER.registerModule(new Object[] { bcontext });
		OntologyManagement.getInstance().register(moduleContext, new HumiditySensorOntology());
		OntologyManagement.getInstance().register(moduleContext, new SPLSlowSensorOntology());
		OntologyManagement.getInstance().register(moduleContext, new SPLFastSensorOntology());
		
		//gui = new SubscriberGUI(this);
		subscriber = new Subscriber(moduleContext);
	}

	public void stop(BundleContext arg0) throws Exception {
		if(subscriber != null) {
			subscriber.close();
			subscriber = null;
		}
		
		OntologyManagement.getInstance().unregister(moduleContext, new HumiditySensorOntology());
		OntologyManagement.getInstance().unregister(moduleContext, new SPLSlowSensorOntology());
		OntologyManagement.getInstance().unregister(moduleContext, new SPLFastSensorOntology());
	}

}
